# Introduction

Il y a une vidéo dans le projet pour expliquer en 2 minutes à quoi sert RMarkdown  : Introduction-rmarkdown.mp4

[Site Rmarkdown - introduction](https://rmarkdown.rstudio.com/lesson-1.html)

# Installation

Ce modèle a été créé par Jean-Christophe HENRY jean.christophe.henry.pro@gmail.com

Vous avec un aperçu du rendu dans 4 formats : Docx, html, pdf, md, vous pouvez essayer d'executer le html dans un navigateur, le code est statique.

Pour utiliser le modèle, il faut télécharger et installer le language R et l'IDE Rstudio (c'est un outil opensource crossplateform et cloud, il y a une version server aussi).

Une fois installé ouvrer le fichier avec l'extension **.Rproj** pour importer le projet dans Rstudio

# Rédaction

Dans le projet j'ai ajouté un fichier : manuel-rmarkdown-cheatsheet-2.0.pdf pour savoir utiliser par exemple une image, une formule mathématique...

Pour la rédaction du document, séléctionner dans l'un des bloc de Rstudio à droite dans l'onglet Files, le fichier portant l'extension **.Rmd** en double cliquant sur celui-ci pour l'ouvrir, ce fichier est un RMarkdown, tout ce passera avec lui.

Pour les images, documents joint et tout le reste ça fonctionne un peut comme la plupart des projets de développement, c'est-à-dire qu'on créer un dossier images dans lesquelles on met nos images puis dans le fichier RMarkdown on met le lien relatif pour les fichiers qu'on souhaite importer.

# Ajout des packages

Penser à installer les packages déclarés dans les premières lignes du fichier **.Rmd** lorsqu'ils seront demandés pour la compilation au moment de faire des exports du document, A utiliser dans la console de R pour l'installation :

install.packages("shiny")

tinytex::install_tinytex

install.packages("knitr")

# Git

Penser à remplacer à la racine du dossier les informations pour git pour éviter de faire un commit sur mon dépôt :P
Dans RStudio, vous pouvez configurer votre dépôt pour faire des commits est push

# Problème PDF

Si problème avec latex ou la compilation vers un fichier pdf alors essayer :

fmtutil-sys --all

