

\\pretitle{\\begin{center}

\\}





















<!-- configuration par défaut du document -->
<!-- on supprime le premier numéro pour la page de gadre -->
<!-- fin de page -->
<!-- seconde page avec la tables des matières -->
<!-- fini de page -->
<!-- La liste des figures -->
<!-- fini de page -->
<!-- On remet le nombre en bas de page pour toutes les pages -->
I. Résumé (EXEMPLE)
===================

Dans ce document, je présente mon état de l’art sur la résilience des
logiciels dits « Moderne ». Nous y trouverons son origine suivie des
méthodes indiquant les démarches pour que le logiciel et son écosystème
fassent de la résilience.

II. Introduction (EXEMPLE)
==========================

Aujourd’hui, nous pouvons observer qu’un e-commerce fonctionne sans
interruption par rapport aux modèles des organisations antérieures. On
constate une augmentation des entreprises utilisatrices de l’ingénierie
de la résilience logicielle en tant que stratégie, afin d’améliorer la
qualité des logiciels qui sont de plus en plus complexes et distribués,
tout en diminuant l’impact d’une défaillance. Il est important de
prendre en compte que les technologies et les architectures évoluent
régulièrement. Je pense notamment à l’émergence ces dernières années du
cloud-native, des micro-services et des chatbots. Nous savons que les
petites défaillances à un endroit critique peuvent paralyser les
opérations et peuvent avoir un impact sur le business.

La résilience logicielle est relativement nouvelle, il existe très peu
d’exemples (pour ceux trouvés, c’est souvent venant d’entreprises comme
Google ou Microsoft pour attirer le client à utiliser leurs services
cloud), bien qu’il existe des informations intéressantes sur quelques
études, congrès, conférences et publications plutôt orientés dans
l’ngénierie système depuis 3, 4 ans.

Il est important d’examiner et d’identifier ce qui est récent et
suggérer une pratique, méthode ou procédé le plus indiqué parmis toutes
les informations recencé sur chaque partie traitée, c’est pourquoi ce
document se présente en deux parties, la première concerne l’origine de
la récente intégration du génie logiciel dans l’ingénierie de la
résilience, puis la seconde illustre la description de la résilience
logicielle et ses pratiques.

Pour cette étude, je me suis inspiré des travaux de plusieurs figures du
milieu datant de 2017 jusqu’à novembre dernier :

| Prénom et nom                  | Partie traitée                                  |
|--------------------------------|-------------------------------------------------|
| Erik Hollnagel et David Wood   | Resilience engineering                          |
| John Allspaw                   | Resilience engineering and software engineering |
| Uwe Friedrichsen               | Resilient Software Design                       |
| Nora Jones                     | Designing Services for Resilience               |
| Bill Curtis et le CISQ         | Consortium for Information & Software Quality   |
| Long Zhang et Martin Monperrus | Automated resilience improvement                |

Au début de cette étude il à été très facile de trouver différents
documents pour implémenter une librairie ou solution résiliente à un
problème défini sur le logiciel. Cependant, il a été difficile de
trouver des informations pour répondre à la question : comment mettre en
œuvre une résilience dans toutes ces différentes approches, avec un plan
à suivre ?

Il m’a semblé important de m’intéresser aussi aux bonnes pratiques en
termes de qualité, c’est pourquoi j’ai consulté le CISQ[(1)](#reference)
(Consortium for Information & Software Quality) concernant la résilience
d’un logiciel pour appuyer ou vérifier les différentes informations
récoltées à travers certains documents scientifiques ou articles de
blogs ou même lors de webconférences.

Dans cet état de l’art, je présente deux approches l’une consiste à
s’intéresser à l’environnement du logiciel et ce qui entre en
interaction avec lui. Ceci sur la façon cognitive de travailler, souvent
indiqué dans notre domaine de l’ingénierie de la résilience. La seconde
s’intéresse plutôt à l’aspect technique voire conceptuel du logiciel par
les architectes et développeurs, c’est à dire ce qui permet au logiciel
d’effectuer la résilience.

<!-- fin de page -->
III. L’ingénierie de la résilience (EXEMPLE)
============================================

D’après le Professeur **Erik Hollnagel** de l’Université de Jönköping en
Suède[(2)](#reference) : « Un système est résilient s’il peut ajuster
son fonctionnement avant, pendant ou après des événements (changements,
perturbations et opportunités), et ainsi maintenir les opérations
requises dans des conditions attendues et inattendues. »

Le terme « Resilience Engineering » voit le jour en 2006 dans le livre
Resilience Engineering: Concepts and Precepts[(3)](#reference), les
auteurs indiquent que l’ingénierie de la résilience correspond à la
résilience des organisations qui conçoivent et exploitent des systèmes
d’ingénierie et non avec les systèmes eux-mêmes. C’est un domaine
d’étude apparu en l’an 2000, il a son origine du génie des systèmes
cognitifs, bien avant la résilience logicielle. C’était surtout en
réponse à quelques accidents à la fin des années 90 que la NASA a pu
rencontrer.

L’ingénierie de la résilience est composée d’une communauté comptant des
praticiens et des chercheurs de :

-   Facteurs humains et Ergonomie

-   Ingénierie des systèmes cognitifs

-   Science de la Sécurité, Complexité

-   Ingénieries au sens large

-   Psychologie cognitive

-   Biologie, Écologie, Sociologie

-   Recherche opérationnelle

-   Cybernétique

C’est une communauté travaillant dans des domaines tels que :

-   Pédiatrie, Chirurgie, Anesthésie

-   Organismes des lois, renseignements

-   Espace, Aviation/ATM

-   Explosifs, Exploitation minière, Construction

-   Lutte contre l’incendie

-   Réseau électrique et distribution

-   Agences militaires

Notre domaine du **génie logiciel** est apparu très récemment dans cette
communauté.

<!-- fin de page -->
Voici un aperçu des visages de l’ingénierie de la résilience :

<!-- image -->

![Comunauté de l’ingénierie de la
résilience](img/comunaute_resilience.png)

Depuis le commencement, il y a eu sept colloques de l’ingénierie de la
résilience en 12 ans : [**Symposia
papers**](https://www.resilience-engineering-association.org/resources/symposium-papers/).

<!-- fin de page -->
<!-- image -->

![Modèle - vue inclusive du système](img/model_system_resilience.png)

<!-- Fin de la page -->
<!-- sous section -->

B. sous-titre (EXEMPLE)
-----------------------

<!-- image -->

<img src="img/lifecycle-incident-632x469.png" alt="Cycle de vie d'un incident." width="55%" />
<p class="caption">
Cycle de vie d’un incident.
</p>

<!-- fin de page -->
IV. Références (EXEMPLE)
========================

Termes pour la recherche:

**software resilience, design resilience software, software fault
tolerance, fault injection, dynamic analysis, exceptionhandling, Failure
Mode and Effects Analysis, FMEA, SRE, Site Reliability Engineering.**

``

**1:** Hollnagel E, Woods D & Leveson, N. C. (Eds.) (2006). Resilience
engineering:  
Concepts and precepts. Aldershot, UK: Ashgate.

**2:** Erik Hollnagel. Resilience Engineering ideas
resilience-engineering

**3:** CISQ Consortium for Information & Software Quality - cisq.org

**4:** John Allspaw Etsy & AdaptiveCLabs Conf London 2019 - Amplifying
Sources of Resilience

**5:** David Wood 2006: Resilience Engineering: Redefining the Culture
of Safety and Risk Management

**6:** David Woods 2003: How Resilience Engineering Can Transform NASA’s
Approach to Risky Decision Making

**9:** STELLA Report from the SNAFUcatchers Workshop on Coping With
Complexity Brooklyn NY, March 14-16, 2017 site

**10:** Tu Pham, CTO at Eway & AdFlex, Co-Founder of DYNO

M. T. Nygard, 2007: Release It!, Pragmatic Bookshelf U. Friedrichsen:
Patterns of Resilience U. Friedrichsen: Resilience reloaded - more
resilience patterns A. Tanenbaum, M. c. Steen, 2006: Systèmes distribués
- Principes et paradigmes, Prentice Hall

<!-- fin de page -->
Sitographie (EXEMPLE)
---------------------

-   [**Erik Hollnagle**](https://erikhollnagel.com/books.html) - Erik
    Hollnagel fait partie des personnes importante et reconnue dans
    l’ingénierie de la résilience, il est actuellement professeur
    principal de sécurité des patients à l’université de Jönköping, en
    Suède et CEO de l’Institute of Resilient Systems à Seoul, en Koré.

-   [**Cornell University Archive**](https://arxiv.org/) - Libre accès à
    1 646 007 documents dans divers domaines. Les soumissions à arXiv
    doivent être conformes aux normes universitaires de l’Université
    Cornell.

-   [**Doc Microsoft**](https://docs.microsoft.com)

-   [**Springer**](https://www.springer.com/fr) - Leaders mondiaux dans
    les domaines scientifiques, techniques et médicaux, fournisseur de
    documents aux chercheurs des universités etc…

-   [**Symposia
    papers**](https://www.resilience-engineering-association.org/resources/symposium-papers/) -
    L’ingénierie de la résilience est une perspective
    transdisciplinaire, afin de permettre aux systèmes techniques et aux
    organisations de la société complexe de fonctionner de manière
    résiliente (adapté de Hollnagel, 2019).

-   [**Zenodo**](http://zenodo.org/) - Par le Cern

-   [**Fisghare**](https://figshare.com/) - Obtenir plus de citations
    pour tous les résultats de recherche universitaire plus de 5000
    citations du contenu de figshare à ce jour.

-   [**HAL**](http://hal.archives-ouvertes.fr/) - Par le CNRS.

-   [**archive.org**](https://archive.org/) - Par Internet Archive
    foundation.

-   [**Osf.io**](https://osf.io/) - [**Cos.io**](https://cos.io/) - Par
    le centre le l’open Science.

-   [**Github**](https://github.com/)

-   [**Libre blanc AWS**](https://aws.amazon.com/fr/whitepapers/)
